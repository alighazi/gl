first step is to just make a tetrs that works with arrow keys just like the version from 10000 year B.C

the second step is to make the tetris not automatic, but give it a set of piece and the player can use mouse to get to arrange the pieces on the board
and fill it (just like the talos principle)

the third step is to add lightning and bump maping to the sigils and also to the board

ther fourth step is to add some GUI for showing text

5. add menu and welcome screen.
6. introduce levels.
7. introduce sound.
8. save progress.

Rendering Ideas
===================
Render the solar system with correct scale and nice lightning with particle effects and all
Precedural Terrain Generation with trees, grass, snow, plants, rocks, sands and water.

TODO
===================

* Remove the current ecs architecutre with inheritance and implement data driven ecs as in: https://github.com/aras-p/dod-playground 
with fixed Structure of Arrays of components and then arbitrary systems with no data that work on the components and just a
integer for EntityID. awesome stuff <3 <3 <3 <3 <3. I first implement that exactly as explained in the presentation and then will
checkout the other resources on the topic:
https://kyren.github.io/2018/09/14/rustconf-talk.html
https://asawicki.info/news_1422_data-oriented_design_-_links_and_thoughts.html
https://www.youtube.com/watch?v=rX0ItVEVjHc
https://docs.google.com/presentation/d/17Bzle0w6jz-1ndabrvC5MXUIQ5jme0M8xBF71oz-0Js/present#slide=id.i0
http://gamesfromwithin.com/data-oriented-design

* add fade effect to the dissapearing blocks: http://antongerdelan.net/blog/formatted/2017_09_04_dissolve.html
* create precompiled header file and add all the common headers there.=> common header files done, precompiling to be added   
* refactor the core folder and in general the whole directory structure. => done
* introduce the notion of models, only models are allowed to have vertices and one instance of a model is enough for example for a block and or for a Sigil, no need to instantiate so many vertices for every block or sigil :). so we need to add something like a `model` class. each object holds a (weak?) reference to a model :) => done
* use smart pointers where apropriate :)
* put everything under my own namespace =>done
* what I have done in 2+ months with tons of dependencies and shit is already done under 1k js https://js1k.com/2019-x/details/4076
* implement Entity compontet System architecture.
* use glitters structure for cmake and dependencies so I can easily builld with windows =>
* rename .h file to .hpp
* check for collision on rotations as well. => done
* display sigil preview in the gui section.
* move everything engine related to a seprate namespace
* remove dependency to assimp and use https://github.com/syoyo/tinyobjloader instead. we can alway convert other formats to obj using http://3doc.i3dconverter.com/
* add a mesh class that holds mesh information. a model can on the other hand include a mesh and other data on top about how to render this mesh, like the material, shader etc.?
* add a scene graph class and use that to draw the scene and decorations (loadable from json/bin file?)