dependencies: 
GLFW,
GLAD,
glm,
fmt,
stb,
assimp

they all should build automatically because they are included as submodules in the git reposotory.
You need to do `git clone --recursive` so the submodules also get cloned. If you forgot to do that when cloning you can always do `git submodule update --init` to fetch the submodules.
then  just do: `cd build` -> `cmake ..` then you can just `make` on linux or open the generated .sln file in visual studio to build and run.
