#include "catch.hpp"
#include "core/cube.h"

using namespace chetris;

TEST_CASE("cube specs"){
    cube c(vec3(1.f, 2.f, 3.f), vec3(4.f, 5.f, 6.f));

    SECTION("Getters and Setters"){
        c.width(94.f);
        REQUIRE(c.width() == 94.f);

        c.height(93.f);
        REQUIRE(c.height() == 93.f);

        c.depth(92.f);
        REQUIRE(c.depth()==92.f);
    }

    SECTION("Move"){
        auto size = vec3(c.size);
        c.move(vec3(100.f, 200.f, 300.f));
        REQUIRE(c.pos == vec3(101.f, 202.f, 303.f));
        REQUIRE(c.size == size);

        c.move(vec3(-300.f, -600.f, -900.f));
        REQUIRE(c.pos == vec3(-199.f, -398.f, -597.f));
        REQUIRE(c.size == size);
    }

    SECTION("Colision"){
        c = cube(vec3(11.f), vec3(10.f));
        cube c2(vec3(0.f), vec3(10.f));
        REQUIRE(c.collides(c2) == false);

        c = cube(vec3(0.f), vec3(0.f));
        REQUIRE(c.collides(c2) == true);

        c = cube(vec3(0.f), vec3(1.f));
        REQUIRE(c.collides(c2) == true);
        
        c = cube(vec3(0.f), vec3(-1.f));
        REQUIRE(c.collides(c2) == true);
        
        c.pos.x -= 0.5f;
        REQUIRE(c.collides(c2) == false);
    }

    SECTION("Contains for equals size"){
        auto c1 = cube(vec3(0.f), vec3(10.f));
        auto c2 = cube(vec3(0.f), vec3(10.f));

        REQUIRE(c1.contains(c2));
        REQUIRE(c2.contains(c1));

        c2 = cube(vec3(10.f), vec3(-10.f));
        REQUIRE(c1.contains(c2));
        REQUIRE(c2.contains(c1));
    }

    SECTION("Contains for bigger cubes"){
        auto c1 = cube(vec3(0.f), vec3(10.f));
        auto c2 = cube(vec3(0.f), vec3(11.f));

        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == true);

        c2 = cube(vec3(11.f), vec3(-11.f));
        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == true);
    }

    SECTION("Contains for smaller cubes"){
        auto c1 = cube(vec3(0.f), vec3(10.f));
        auto c2 = cube(vec3(0.f), vec3(9.f));

        REQUIRE(c1.contains(c2) == true);
        REQUIRE(c2.contains(c1) == false);

        c2 = cube(vec3(9.f), vec3(-9.f));
        REQUIRE(c1.contains(c2) == true);
        REQUIRE(c2.contains(c1) == false);
    }

    SECTION("Contains for outsider cubes"){
        auto c1 = cube(vec3(0.f), vec3(10.f));
        auto c2 = cube(vec3(11.f), vec3(10.f));

        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == false);

        c2 = cube(vec3(20.f), vec3(-9.f));
        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == false);
    }

    SECTION("Contains for overlapping cubes"){
        auto c1 = cube(vec3(0.f), vec3(10.f));
        auto c2 = cube(vec3(4.f), vec3(10.f));

        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == false);

        c2 = cube(vec3(14.f), vec3(-10.f));
        REQUIRE(c1.contains(c2) == false);
        REQUIRE(c2.contains(c1) == false);
    }
}