#include "catch.hpp"
#include "core/ecs/c_sigil.h"
using namespace chetris;

TEST_CASE("sigil width and height","[.]"){
    SECTION( "width and height of bar is 4 and 1" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_Bar);
        REQUIRE( s.width() == 4);
        REQUIRE( s.height() == 1);
    }
    SECTION("w & h of vertical bar 1, 4"){
        int SIZE = 4;
        static const block_t t = (0b0001 <<SIZE * 3)
                                +(0b0001 <<SIZE * 2)
                                +(0b0001 <<SIZE * 1)
                                +(0b0001 <<SIZE * 0);
        BlockDef bd = BlockDef(t);
        REQUIRE( bd.width() == 1);
        REQUIRE( bd.height() == 4);
    }
    SECTION( "width and height of box is 2 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_Box);
        REQUIRE( s.width() == 2);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of L is 3 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_L);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of LR is 3 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_LR);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of S is 3 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_S);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of SR is 3 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_SR);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of W is 3 and 2" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_W);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "width and height of M is 3 and 3" ) {
        BlockDef s = BlockDef(BlockDef::TYPE_W);
        REQUIRE( s.width() == 3);
        REQUIRE( s.height() == 2);
    }
    SECTION( "All Sigil Types when rotated, their width and heights will swap" ) {
        int count = sizeof(BlockDef::all_types)/sizeof(block_t);
        for(int i = 0; i < count; i++)
        {
            BlockDef bd = BlockDef::all_types[i];
            int width = bd.width();
            int height = bd.height();
            bd = bd.rotate();
            REQUIRE( bd.width() == height);
            REQUIRE( bd.height() == width);

            bd = bd.rotate();
            REQUIRE( bd.width() == width);
            REQUIRE( bd.height() == height);

            bd = bd.rotate();
            REQUIRE( bd.width() == height);
            REQUIRE( bd.height() == width);
        }

    }

}

TEST_CASE("reaaallyy")
{

}