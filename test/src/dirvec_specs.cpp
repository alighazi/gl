#include "catch.hpp"
#include "core/direction.h"

using namespace chetris;

TEST_CASE("dirvec opposite"){
     SECTION("integer 2d direction"){
        idirvec2 d1 = idirvec2::UP;
        idirvec2 d2 = idirvec2::LEFT;
        idirvec2 d3 = idirvec2::RIGHT;
        idirvec2 d4 = idirvec2::DOWN;
        REQUIRE(d1.opposite() == d4);
        REQUIRE(d2.opposite() == d3);
        REQUIRE(d3.opposite() == d2);
        REQUIRE(d4.opposite() == d1);
    }
    SECTION("float 2d direction"){
        dirvec2 d1 = dirvec2::UP;
        dirvec2 d2 = dirvec2::LEFT;
        dirvec2 d3 = dirvec2::RIGHT;
        REQUIRE(d1.opposite() == dirvec2::DOWN);
        REQUIRE(d2.opposite() == d3);
        REQUIRE(d3.opposite() == d2);
        REQUIRE(dirvec2::DOWN.opposite() == d1);
    }
    SECTION("integer 3d direction"){
        idirvec3 d1 = idirvec3::UP;
        idirvec3 d2 = idirvec3::LEFT;
        idirvec3 d3 = idirvec3::FRONT;
        REQUIRE(d1.opposite() == idirvec3::DOWN);
        REQUIRE(d2.opposite() == idirvec3::RIGHT);
        REQUIRE(d3.opposite() == idirvec3::BACK);
    }
    SECTION("float 3d direction"){
        dirvec3 d1 = dirvec3::UP;
        dirvec3 d2 = dirvec3::LEFT;
        dirvec3 d3 = dirvec3::FRONT;
        REQUIRE(d1.opposite() == dirvec3::DOWN);
        REQUIRE(d2.opposite() == dirvec3::RIGHT);
        REQUIRE(d3.opposite() == dirvec3::BACK);
    }
}