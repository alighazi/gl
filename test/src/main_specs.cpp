#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "core/game.h"

chetris::Game &game = chetris::Game::instance(); //important to create one to initialize dependencies;