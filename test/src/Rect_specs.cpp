#include "catch.hpp"
#include "core/rect.h"
#include "core/util/to_string.h"

using namespace chetris;

TEST_CASE("irect Collision"){
    irect r1 = irect(glm::ivec2(10,10), 4, 4);

    SECTION( "With another at top (no collision)" ) {
        irect r2 = irect(glm::ivec2(10,15), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top (touching)" ) {
        irect r2 = irect(glm::ivec2(10,14), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top (colliding)" ) {
        irect r2 = irect(glm::ivec2(10,13), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "With another at bottom (no collision)" ) {
        irect r2 = irect(glm::ivec2(10,5), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom (touching)" ) {
        irect r2 = irect(glm::ivec2(10,6), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom (colliding)" ) {
        irect r2 = irect(glm::ivec2(10,7), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "With another at left (no collision)" ) {
        irect r2 = irect(glm::ivec2(5,10), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at left (touching)" ) {
        irect r2 = irect(glm::ivec2(6,10), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at left (colliding)" ) {
        irect r2 = irect(glm::ivec2(7,10), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "With another at right (no collision)" ) {
        irect r2 = irect(glm::ivec2(15,10), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at right (touching)" ) {
        irect r2 = irect(glm::ivec2(14,10), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at right (colliding)" ) {
        irect r2 = irect(glm::ivec2(13,10), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "With another at top right (no collision)" ) {
        irect r2 = irect(glm::ivec2(15,15), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top right (touching)" ) {
        irect r2 = irect(glm::ivec2(14,14), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top right (colliding)" ) {
        irect r2 = irect(glm::ivec2(13,13), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "With another at bottom right (no collision)" ) {
        irect r2 = irect(glm::ivec2(15,5), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom right (touching)" ) {
        irect r2 = irect(glm::ivec2(14,6), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom right (colliding)" ) {
        irect r2 = irect(glm::ivec2(13,7), 4, 4);
        REQUIRE(r1.collides(r2));
    }

    SECTION( "With another at top left (no collision)" ) {
        irect r2 = irect(glm::ivec2(5,15), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top left (touching)" ) {
        irect r2 = irect(glm::ivec2(6,14), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at top left (colliding)" ) {
        irect r2 = irect(glm::ivec2(7,13), 4, 4);
        REQUIRE(r1.collides(r2));
    }

    SECTION( "With another at bottom left (no collision)" ) {
        irect r2 = irect(glm::ivec2(5,5), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom left (touching)" ) {
        irect r2 = irect(glm::ivec2(6,6), 4, 4);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }
    SECTION( "With another at bottom left (colliding)" ) {
        irect r2 = irect(glm::ivec2(7,7), 4, 4);
        REQUIRE(r1.collides(r2));
    }
    SECTION( "arbitrary (not colliding)" ) {
        r1 = irect(3,4,1,2);
        irect r2 = irect(2,6,0,3);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }

    SECTION( "arbitrary (colliding)" ) {
        r1 = irect(2,6,0,3);
        irect r2 = irect(3,7,1,5);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }

    SECTION( "arbitrary (colliding)" ) {
        r1 = irect(2,6,0,3);
        irect r2 = irect(2,7,0,5);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }
    SECTION( "arbitrary (not colliding)" ) {
        r1 = irect(2,6,0,3);
        irect r2 = irect(4,7,2,5);
        REQUIRE(r1.collides(r2) == false);
		REQUIRE(r2.collides(r1) == false);
    }

    SECTION( "arbitrary (colliding)" ) {
        r1 = irect(3,4,1,1);
        irect r2 = irect(3,5,1,3);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }
    SECTION( "inside aligned with right edge (colliding)" ) {
        r1 = irect(3,4,1,1);
        irect r2 = irect(3,4,1,3);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }
    SECTION( "inside center (colliding)" ) {
        r1 = irect(3,4,1,1);
        irect r2 = irect(3,3,1,2);
        REQUIRE(r1.collides(r2) == true);
		REQUIRE(r2.collides(r1) == true);
    }
}
