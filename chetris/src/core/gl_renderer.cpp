#include <glad/glad.h>
#include "core/util/to_string.h"
#include "core/gl_renderer.h"

GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        std::string error;
        switch (errorCode)
        {
            case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
        }
        fmt::print("{} | {} ({})\n", error, file, line);
    }
    return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__) 

namespace chetris 
{

GlRenderer::~GlRenderer()
{
}

void GlRenderer::dispose() noexcept {
	if(vBO_){
		glDeleteVertexArrays(1, &vAO_);
		glDeleteBuffers(1, &vBO_);
		glDeleteBuffers(1, &eBO_);
		vBO_ = eBO_ = vAO_ = 0;
	}
}

void GlRenderer::loadMeshs(const vector<Mesh*> &meshs){
	
	glGenVertexArrays(1, &vAO_);
	glGenBuffers(1, &vBO_);
	glGenBuffers(1, &eBO_);
	glBindVertexArray(vAO_);
	glBindBuffer(GL_ARRAY_BUFFER, vBO_);

	int total_vertices = 0, total_indices=0;
	for (auto m : meshs) {
		if(offsets.find(m) == offsets.end()){
			total_vertices += m->vertices.size();
			total_indices += m->indices.size();
			offsets.insert({m, {-1,-1}});
		}
	}

	glBufferData(GL_ARRAY_BUFFER, total_vertices * sizeof(Vertex), 0, GL_STATIC_DRAW);

	//Element buffer object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eBO_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, total_indices * sizeof(unsigned), 0, GL_STATIC_DRAW);

	int vBOffset = 0, eBOffset =0;
	for (auto mesh : meshs) {
		auto& offset = offsets.at(mesh);
		if(offset.vBO >= 0) continue;//already loaded
		
		auto &is = mesh->indices;		
		if(is.size()){
			fmt::print("glBufferSubData eBOffset: {}, is.size(): {} \n", eBOffset, is.size());
			//we have to offset all the indexes because the vertices that these indexes point to do not start from 0
			for(auto i=0; i<is.size(); i++){
				is[i]=is[i]+vBOffset;
			}
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, eBOffset * sizeof(unsigned), is.size() * sizeof(unsigned), is.data());
			glCheckError();

			offset.eBO = eBOffset;
			eBOffset += is.size();
		}

		auto &vs = mesh->vertices;
		fmt::print("glBufferSubData vBOffset: {} vs.size(): {}\n", vBOffset, vs.size());
		glBufferSubData(GL_ARRAY_BUFFER, vBOffset * sizeof(Vertex), vs.size() * sizeof(Vertex), vs.data());
		offset.vBO = vBOffset;
		vBOffset += vs.size();		
		glGetError();
	}

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	// normal attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	// texture cordinate attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	// note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0);
	

}

void GlRenderer::loadTextures(const vector<string> &textures){
	for (auto& t : textures) {
		loadTexture(t);
	}
	shader_->setInt("diffuseMap", 0);
	shader_->setInt("normalMap", 1);
}

void GlRenderer::begingRender(Camera& cam) {
	glEnable(GL_DEPTH_TEST);
	glEnablei(GL_BLEND, 0), glClearColor(bgColor.r, bgColor.b, bgColor.g, bgColor.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader_->Use();
	shader_->setMat4("view", cam.getView());
	shader_->setMat4("projection", cam.getProjection());
	shader_->setVec3("camPos", cam.getPos());
}

void GlRenderer::render(const Model *model) {

	auto find = offsets.find(model->mesh.get());
	assert(find != offsets.end());//Model is loaded?	
	auto offset = find->second;
	

	if(model->diffuseMapFn.empty()){
		shader_->setBool("textured", false);
	}else{		
		auto name = textureNames_.find(model->diffuseMapFn);
		assert(name != textureNames_.end());
		shader_->setBool("textured", true);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, name->second);		
	}

	if(model->normalMapFn.empty()){
		shader_->setBool("bumped", false);
	}else{		
		auto name = textureNames_.find(model->normalMapFn);
		assert(name != textureNames_.end());
		shader_->setBool("bumped", true);
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, name->second);		
	}

	auto&& modelMat = transformStack_.top().transform_;
    shader_->setMat4("model", modelMat);
	shader_->setMat3("normalMat", glm::transpose(glm::inverse(modelMat)));	
	
	glBindVertexArray(vAO_);

	shader_->setVec4("uColor", model->colorDiffuse);

	if(model->isWireFrame){
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	}else{
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
	}

	if(model->mesh->indices.size()){
		assert(offset.eBO >= 0);//Model is loaded?	
		glDrawElements(GL_TRIANGLES, model->mesh->indices.size(), GL_UNSIGNED_INT, (void*) (offset.eBO * sizeof(unsigned)));
	}
	else{		
		glDrawArrays(GL_TRIANGLES, offset.vBO, model->mesh->vertices.size());
	}
//	glCheckError();
}

void GlRenderer::loadTexture(const string &fileName){	
	static const string path="data/img/";
	if(textureNames_.find(fileName) != textureNames_.end())
		return; //texture already loaded

	// load and create a texture 
    // texture 1
	unsigned int textureName;
    glGenTextures(1, &textureName);
    glBindTexture(GL_TEXTURE_2D, textureName);
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // load image, create texture and generate mipmaps
	ImageInfo img = ImageInfo(path+fileName);
	if(img.isOk()){
		auto channels = img.numberOfChannels() == 4 ? GL_RGBA : GL_RGB;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width(), img.height(), 0, channels, GL_UNSIGNED_BYTE, img.pixels());
		glGenerateMipmap(GL_TEXTURE_2D);
		img.free();
	}else {
		fmt::print("Failed to load texture: {}\n", img.getFileName());
		return;
	}
	textureNames_.insert({fileName, textureName});
}

void GlRenderer::setShader(const string &vertexFileName, const string &fragmentFileName){
    // build and compile our shader_ program
    shader_ = std::make_unique<Shader>(vertexFileName, fragmentFileName);
    shader_->Use();
}

void GlRenderer::pushTransform(const Transform &transform){
	transformStack_.emplace(transformStack_.top().transform_ * transform.transform_);
}

void GlRenderer::popTransform(){
	assert(transformStack_.size()>1);
	transformStack_.pop();
}

}//namespace
