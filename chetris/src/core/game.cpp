#include <experimental/vector>
#include <ctime>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>
#include "core/game.h"
#include "core/gl_renderer.h"
#include "core/ecs/c_block.h"
#include "core/ecs/c_sigil_physics.h"
#include "core/ecs/c_sigil_control.h"
#include "core/util/instance_counter.h"

namespace chetris {
	using ecs::Group;

	Game::Game() noexcept {}

	void Game::start(int windowWidth, int windowHeight) {
		windowSize_ = ivec2(windowWidth, windowHeight);
		// glfw: initialize and configure
		// ------------------------------
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	// glfw window creation
	// --------------------
		window = glfwCreateWindow(windowSize_.x, windowSize_.y, "Chetris", nullptr, nullptr);
		if (window == nullptr)
		{
			fmt::print("Failed to create GLFW window\n");
			glfwTerminate();
			exit(-1);
		}
		glfwMakeContextCurrent(window);
		glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
		glfwSetKeyCallback(window, key_callback);
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetCursorPosCallback(window, mouse_callback);

		// glad: load all OpenGL function pointers
		// ---------------------------------------
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			fmt::print("Failed to initialize GLAD\n");
			exit(-1);
		}

		gui_ = make_unique<Gui>(irect(ivec2(0, 0), windowSize_.x / (frame_.width() / guiAreaWidth_ + 1.0), windowSize_.y));
		sigilSpawner_.init(manager_, frame_, guiAreaWidth_);

		camera_ = make_unique<Camera>(windowSize_.x, windowSize_.y);
		GlRenderer::instance().setShader("chetris/shader/default.vert", "chetris/shader/default.frag");

		box = make_shared<Model>(Model{Mesh::cube(), "brickwall.jpg","brickwall_normal.jpg", vec4(1.f)});
		lamp = make_shared<Model>(Model{Mesh::cube(), "", "", vec4(1.f)});
		ring = make_shared<Model>(Model{Mesh::generate_torus(36, 9, 4.f, 1.f), "", "", vec4(1.f,0.5f,0.1f,1.f)});


		//TODO: we need a resource manager class here
		GlRenderer::instance().loadMeshs(make_vector(box->mesh.get(), ring->mesh.get()));
		GlRenderer::instance().loadTextures(make_vector<string>("box_border.png", "wud.jpg", "face.png",
		 "brickwall.jpg", "brickwall_normal.jpg"));

		ecs::Entity& e = manager_.addEntity();
		e.addGroup(Group::NEXT_SIGILS);

		for (int i = 0; i < frame_.width(); i++)
		{
			for (int j = -1; j >= frame_.depth(); j--)
			{
				auto& floor = manager_.addEntity();
				floor.addGroup(Group::FRAME);
				floor.addComponent<CBlock>(ivec3(i, -1, j), vec3(0.3f));
			}
		}

		//lets build the frame
		vec4 frameColor(1.f, 1.f, 1.f, 0.4f);
		for (int i = -1; i <= frame_.height(); i++)
		{
			auto& leftFront = manager_.addEntity();
			leftFront.addGroup(Group::FRAME);
			leftFront.addComponent<CBlock>(ivec3(-1, i, 0), frameColor);

			auto& rightFront = manager_.addEntity();
			rightFront.addGroup(Group::FRAME);
			rightFront.addComponent<CBlock>(ivec3(frame_.width(), i, 0), frameColor);

			auto& leftBack = manager_.addEntity();
			leftBack.addGroup(Group::FRAME);
			leftBack.addComponent<CBlock>(ivec3(-1, i, frame_.depth() - 1), frameColor);

			auto& rightBack = manager_.addEntity();
			rightBack.addGroup(Group::FRAME);
			rightBack.addComponent<CBlock>(ivec3(frame_.width(), i, frame_.depth() - 1), frameColor);
		}

	}

	bool Game::isRunning() noexcept {
		return !glfwWindowShouldClose(window);
	}

	void Game::dispose() noexcept {
		GlRenderer::instance().dispose();
		gui_.reset();
		glfwTerminate();
	}

	Game::~Game() noexcept {
		dispose();
	}

	void Game::key_callback(GLFWwindow* window, int key, [[maybe_unused]] int scancode,
		[[maybe_unused]] int action, [[maybe_unused]] int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);
		Game::instance().keysPressed_[key] = action == GLFW_PRESS || action == GLFW_REPEAT;
		Game::instance().manager_.forAll<CSigilControl>(
			[&](unique_ptr<ecs::Entity>& e) {
				auto c = e->getComponent<CSigilControl>();
				switch (action)
				{
				case GLFW_REPEAT:
					c.onKeyPress(key, true);
					break;
				case GLFW_PRESS:
					c.onKeyPress(key, false);
					break;
				case GLFW_RELEASE:
					c.onKeyRelease(key);
					break;
				default:
					break;
				}
			});
	}

	void Game::mouse_callback(GLFWwindow*, double xpos, double ypos)
	{
		Game::instance().camera_->lookAt(xpos, ypos);
	}

	// glfw: whenever the window size changed (by OS or user resize) this callback function executes
	void Game::framebuffer_size_callback(GLFWwindow*, int width, int height)
	{
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		Game::instance().windowSize_ = ivec2(width, height);
		Game::instance().gui_->bounds(irect(ivec2(0, 0), glm::floor(width * 0.25), height));
		Game::instance().camera_->resizeWindow(width, height);
	}

	void Game::update() {
		static float currentSecond;
		static int fps;
		const double currentFrame = glfwGetTime();
		deltaTime_ = currentFrame - lastFrame_;
		lastFrame_ = currentFrame;
		currentSecond += deltaTime_;
		fps++;
		if (currentSecond >= 1) {
			gui_->fps(fps);
			currentSecond = 0;
			fps = 0;
		}
		glfwPollEvents();
		camera_->update(deltaTime_);
		manager_.refresh();

		if (!gameEnd_) {

			manager_.update(deltaTime_);

			auto& sigils = manager_.getEntitiesByGroup(Group::SIGIL);
			if (sigils.empty()) {
				checkRowCompletion();
				auto spawned = sigilSpawner_.spawn();
				CSigilPhysics physics = spawned->getComponent<CSigilPhysics>();
				if (physics.checkCollision()) {
					spawned->destroy();
					stats_.gameEnd(true);
					gameEnd_ = true;
					fmt::print("Game End!");
				}
			}
			else {
				auto& entity = sigils[0];
				CSigil& sigil = entity->getComponent<CSigil>();
				if (!entity->isAlive()) {
					//entity just got stuck, so time to merge
					//maybe it would be cleaner if we had used a dedicated flag 
					//for marking the sigil as stuck. but its good for now
					mergeSigil(sigil);
				}
				else {
					highlightBlocks(sigil);
				}
			}
			gui_->stats(stats_);
		}
	}

	void Game::mergeSigil(CSigil& sigil) {
		for (auto&& b : sigil.blocks()) {
			auto& blockEntity = manager_.addEntity();
			blockEntity.addGroup(Group::BLOCK);
			blockEntity.addComponent<CBlock>(b->position() + ivec3(sigil.position()), b->color());
		}
	}

	void Game::highlightBlocks(CSigil& sigil) {
		auto xzs = unordered_set<vec2>();
		for (auto&& b : sigil.blocks()) {
			auto pos = b->position() + ivec3(sigil.position());
			xzs.emplace(vec2(pos.x, pos.z));
		}
		unordered_map<vec2, CBlock*> highlighted;
		manager_.forAll<CBlock>([&](unique_ptr<ecs::Entity>& e) {
			auto&& cb = e->getComponent <CBlock>();
			vec2 xz(cb.position().x, cb.position().z);
			//candid for highlighting? 
			cb.highlight(false);
			if (xzs.find(xz) != xzs.end()) {
				//already highlighted on same column
				if (highlighted.find(xz) == highlighted.end()) {
					cb.highlight(true);
					highlighted[xz] = &cb;
				}
				else if (highlighted[xz]->position().y < cb.position().y) {
					cb.highlight(true);
					highlighted[xz]->highlight(false);
					highlighted[xz] = &cb;
				}

			}
			});
	}

	void Game::checkRowCompletion() {
		unordered_map<vec2, unsigned> rowSizes;

		auto blocksVector = manager_.getEntitiesByGroup(Group::BLOCK);
		auto blocks = unordered_map<ivec3, ecs::Entity*>();
		for (auto&& e : blocksVector) {
			auto b = e->getComponent<CBlock>();
			blocks[b.position()] = e;
			auto yz = vec2(b.position().y, b.position().z);
			if (rowSizes.find(yz) == rowSizes.end())
				rowSizes[yz] = 1;
			else
				rowSizes[yz]++;
			//here there is a possible array out of bounds eerror when an sigil starts from over the top of the board and lands immediately
		}

		int linesCleared = 0;
		for (int z = frame_.depth() - 1; z < 0; z++) {
			for (int y = frame_.height() - 1; y >= 0; y--) {
				vec2 v(y, z);
				if (rowSizes.find(v) == rowSizes.end()) continue;//nothing at this row
				if (rowSizes[v] != frame_.width()) continue; //row not complete;
				linesCleared++;
				for (int x = 0; x < frame_.width(); x++) {
					auto&& key = ivec3(x, y, z);
					fmt::print("{}|", key);
					auto block = blocks[key];

					block->destroy();
					blocks.erase(key);
				}

				//all the blocks in the rows above need to go one step down
				for (int y = v.x + 1; y < frame_.height() - 1; y++) {
					for (int x = 0; x < frame_.width(); x++) {
						auto&& key = ivec3(x, y, z);
						auto kv = blocks.find(key);
						if (kv == blocks.end()) continue;

						CBlock& block = kv->second->getComponent<CBlock>();
						ivec3 newPosition(block.position() - ivec3(0, 1, 0));
						block.position(newPosition);
						blocks[newPosition] = kv->second;
						blocks.erase(key);
					}
				}
			}
		}
		stats_.addLines(linesCleared);
	}

	void Game::render() {
		auto& rdr = GlRenderer::instance();
		rdr.begingRender(*camera_);

		auto anim1 = glm::sin(lastFrame_ / 2.f) * 0.5 + 0.5f;
		auto anim2 = glm::cos(lastFrame_ / 2.f) * 0.5 + 0.5f;
		auto lightPos = vec3((anim1 * frame_.width() + guiAreaWidth_) - guiAreaWidth_, anim2 * frame_.height(), 2.f)*3.f;
		rdr.shader().setVec3("lightPos", lightPos);
		manager_.render();

		Transform t(lightPos);
		rdr.pushTransform(t);
		rdr.render(lamp.get());
		rdr.popTransform();

		t = Transform(vec3(6.f, 0.f, 1.f));
		//t.rotate(vec3(0.f, 0.f, 1.f), lastFrame_);
		rdr.pushTransform(t);
		rdr.render(ring.get());
		rdr.popTransform();

		t = Transform(vec3(0.f, 0.f, +02.f));
		//t.scale(vec3(10.f));
		rdr.pushTransform(t);
		rdr.render(box.get());
		rdr.popTransform();


		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		/*gui should be renedered last, it modifies the global opengl state
		 so we should set the correct state at the start of each frame*/
		gui_->render();
		glfwSwapBuffers(window);
	}
}//namespace
