#include "core/direction.h"
namespace chetris {
	using glm::vec3;
	using glm::vec2;
	using glm::ivec3;
	using glm::ivec2;

	template<>
	const dirvec3 dirvec3::NONE = vec3(0.f);
	template<>
	const dirvec3 dirvec3::RIGHT = vec3(1.0f, 0.0f, 0.0f);
	template<>
	const dirvec3 dirvec3::UP = vec3(0.0f, 1.0f, 0.0f);
	template<>
	const dirvec3 dirvec3::DOWN = vec3(0.0f, -1.f, 0.0f);
	template<>
	const dirvec3 dirvec3::LEFT = vec3(-1.f, 0.0f, 0.0f);
	template<>
	const dirvec3 dirvec3::FRONT = vec3(0.0f, 0.0f, 1.f);
	template<>
	const dirvec3 dirvec3::BACK = vec3(0.0f, 0.0f, -1.0f);

	template<>
	const dirvec2 dirvec2::NONE = vec2(0.f);
	template<>
	const dirvec2 dirvec2::RIGHT = vec2(1.0f, 0.0f);
	template<>
	const dirvec2 dirvec2::UP = vec2(0.0f, 1.0f);
	template<>
	const dirvec2 dirvec2::DOWN = vec2(0.0f, -1.f);
	template<>
	const dirvec2 dirvec2::LEFT = vec2(-1.f, 0.0f);

	template<>
	const idirvec3 idirvec3::NONE = ivec3(0);
	template<>
	const idirvec3 idirvec3::RIGHT = ivec3(1, 0, 0);
	template<>
	const idirvec3 idirvec3::UP = ivec3(0, 1, 0);
	template<>
	const idirvec3 idirvec3::DOWN = ivec3(0, -1, 0);
	template<>
	const idirvec3 idirvec3::LEFT = ivec3(-1, 0, 0);
	template<>
	const idirvec3 idirvec3::FRONT = ivec3(0, 0, 1);
	template<>
	const idirvec3 idirvec3::BACK = ivec3(0, 0, -1);

	template<>
	const idirvec2 idirvec2::NONE = ivec2(0);
	template<>
	const idirvec2 idirvec2::RIGHT = ivec2(1, 0);
	template<>
	const idirvec2 idirvec2::UP = ivec2(0, 1);
	template<>
	const idirvec2 idirvec2::DOWN = ivec2(0, -1);
	template<>
	const idirvec2 idirvec2::LEFT = ivec2(-1, 0);
}
