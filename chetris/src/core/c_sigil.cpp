#include "core/ecs/c_sigil.h"
#include "core/gl_renderer.h"

namespace chetris
{

CSigil::CSigil(const BlockDef definition, const vec3 &pos, float scale) 
:definition_(definition), position_(pos), scale_(scale) {
	initBlocks();
	updateTransform();
}

void CSigil::initBlocks() {
	float r1 = (std::rand() % 1000) / 1000.f;
	float r2 = (std::rand() % 1000) / 1000.f;
	float r3 = (std::rand() % 1000) / 1000.f;
	auto color = vec3(r1, r2, r3);
	auto &&def = definition_.ones();
	blocks_.clear();
	for (const ivec2 &p : *def) {
		blocks_.push_back(std::make_unique<CBlock>(ivec3(p,0), color));
	}
}

void CSigil::render() {
	GlRenderer::instance().pushTransform(transform_);
	for (auto&& b : blocks_)
		b->render();
	GlRenderer::instance().popTransform();
}

void CSigil::position(const vec3 &position) {
	position_ = position;
	updateTransform();
}

void CSigil::scale(float scale){
	scale_ = scale;	
	updateTransform();
}

void CSigil::updateTransform(){
	bounds_ = cube(position_, vec3(width(), height(), depth()));
	
	transform_ = Transform();
	transform_.translate(position_);
	transform_.scale(vec3(scale_));
}

void CSigil::update(float) {
}

CSigil::~CSigil() {
}

int CSigil::width() {
	return definition_.width() * scale_;
}

int CSigil::height() {
	return definition_.height() * scale_;
}

int CSigil::depth(){
	return 1 * scale_;
}

void CSigil::move(ivec3 dir) {
	fmt::print("moving by: {}\n", dir);
	position_ += dir;
	updateTransform();
}

void CSigil::dump() {
	fmt::print("position: {}\n", position_);
	fmt::print("definition:\n");
	definition_.dump();
	for (auto&& b : blocks_) {
		fmt::print("block, pos: {}\n", b->position());;
	}
}

bool CSigil::collidesWith(CBlock *b) {
	for (auto&& myB : blocks_)
	{
		if (b->position() == myB->position() +  ivec3(position_))
			return true;
	}
	return false;

}

void CSigil::rotate() {
	fmt::print("before rotate:\n");
	definition_.dump();
	fmt::print("blocks: {}\n", blocks_.size());
	definition_ = definition_.rotate();
	initBlocks();
	fmt::print("after rotate:\n");
	definition_.dump();
	fmt::print("blocks: {}\n", blocks_.size());
	updateTransform();
}

void CSigil::undoRotate(){
	fmt::print("before undoRotate:\n");
	definition_.dump();
	fmt::print("blocks: {}\n", blocks_.size());
	definition_ = definition_.rotate();
	definition_ = definition_.rotate();
	definition_ = definition_.rotate();
	initBlocks();
	fmt::print("after undoRotate:\n");
	definition_.dump();
	fmt::print("blocks: {}\n", blocks_.size());
	updateTransform();
}

}//namespace
