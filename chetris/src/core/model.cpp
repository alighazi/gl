#include "core/model.h"

namespace chetris
{

	shared_ptr<Mesh> Mesh::cube() {
		static shared_ptr<Mesh> cube;

		if (cube == nullptr) {
			cube = make_shared<Mesh>();

			vec3 scale = vec3(1.f);
			vec3 translate = vec3(0.5f, 0.5f, 0.f);

			auto size = sizeof(cube_vertices) / sizeof(float);
			auto arr = cube_vertices;
			for (auto i = 0u; i < size; i += 8) {
				Vertex v{
					(vec3(arr[i], arr[i + 1], arr[i + 2]) * scale) + translate,
					vec3(arr[i + 3], arr[i + 4], arr[i + 5]),
					vec2(arr[i + 6], arr[i + 7]) };
				cube->vertices.push_back(v);
			}
		}
		return cube;
	}

	shared_ptr<Mesh> Mesh::generate_torus(unsigned sectionsBig, unsigned sectionsSmall, float radiusBig, float radiusSmall) {
		shared_ptr<Mesh> m = make_shared<Mesh>();
		for (unsigned s1 = 0; s1 <= sectionsBig; s1++) {
			float teta = glm::radians(s1 * (360.f / sectionsBig));
			vec3 s1Center(glm::cos(teta), 0.f, glm::sin(teta));
			s1Center *= radiusBig;
			float texX = s1 * 1.f / sectionsBig;
			for (unsigned s2 = 0; s2 <= sectionsSmall; s2++) {
				float gama = glm::radians(s2 * (360.f / sectionsSmall));
				auto xz = glm::cos(gama) * glm::normalize(s1Center);
				auto normal = vec3(xz.x, glm::sin(gama), xz.z);
				vec3 s2Center = s1Center + normal * radiusSmall;
				vec2 textCoords(texX, s2 * 1.f / sectionsSmall);
				m->vertices.emplace_back(Vertex{s2Center, normal, textCoords });
			}
		}

		for (unsigned i = 0; i < sectionsBig; i++) {
			int r1 = i * (sectionsSmall + 1);
			int  r2 = (i + 1) * (sectionsSmall + 1);
			for (unsigned j = 0; j < sectionsSmall; j++) {
				m->indices.push_back(r1 + j);
				m->indices.push_back(r2 + j +1);
				m->indices.push_back(r2 + j );

				m->indices.push_back(r1 + j);
				m->indices.push_back(r1 + j + 1);
				m->indices.push_back(r2 + j + 1);
			}
		}
		return m;
	}

	shared_ptr<Mesh> Mesh::generate_ring(unsigned sections, float radius, float height) {
		shared_ptr<Mesh> m = make_shared<Mesh>();
		if (sections < 3)
			return m;//empty

		for (unsigned s = 0; s <= sections; s++) {
			float teta = glm::radians(s * (360.f / sections));
			auto center = vec3(glm::cos(teta), 0.f, glm::sin(teta));
			auto normal = glm::normalize(center);
			center *= radius;
			m->vertices.emplace_back(Vertex{ vec3(center.x, +height / 2.f, center.z), normal,
			vec2(static_cast<float>(s) / sections, 1.f) });
			m->vertices.emplace_back(Vertex{ vec3(center.x, -height / 2.f, center.z), normal,
			vec2(static_cast<float>(s) / sections, 0.f) });
		}

		m->indices.push_back(0);
		m->indices.push_back(1);
		m->indices.push_back(2);
		for (unsigned s = 1; s < sections; s++) {
			auto i = s * 2;

			m->indices.push_back(i);
			m->indices.push_back(i - 1);
			m->indices.push_back(i + 1);

			m->indices.push_back(i);
			m->indices.push_back(i + 1);
			m->indices.push_back(i + 2);
		}
		auto i = sections * 2;
		m->indices.push_back(i);
		m->indices.push_back(i - 1);
		m->indices.push_back(i + 1);

		return m;
	}

	shared_ptr<Mesh> Mesh::generate_cylinder(unsigned sections, float radius, float height) {
		auto m = Mesh::generate_ring(sections, radius, height);

		auto normal = glm::normalize(vec3(0.f, 1.f, 0.f));
		m->vertices.emplace_back(Vertex{ vec3(0.f, +height / 2.f, 0.f), +normal, vec2(1.f, 1.f) });
		auto iCenterTop = m->vertices.size() - 1;
		m->vertices.emplace_back(Vertex{ vec3(0.f, -height / 2.f, 0.f), -normal, vec2(0.f, 0.f) });
		auto iCenterBottom = m->vertices.size() - 1;
		for (unsigned s = 0; s < sections; s++) {
			m->indices.push_back(s * 2);
			m->indices.push_back(iCenterTop);
			m->indices.push_back((s + 1) * 2);

			m->indices.push_back(s * 2 + 1);
			m->indices.push_back(iCenterBottom);
			m->indices.push_back((s + 1) * 2 + 1);
		}

		return m;
	}

	void Mesh::generate_sphere() {
	}
}
