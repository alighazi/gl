#include "core/ecs/c_sigil_physics.h"
#include "core/game.h"

namespace chetris
{
   
    void CSigilPhysics::update(float dt){
        static float last_frame=0.f;
        last_frame += dt;

        if(last_frame < Game::instance().stats().frameTime())
            return;

        if(!moveSigil(idirvec3::DOWN)){
            entity->destroy();
        }
        last_frame = 0.f;
    }

    bool CSigilPhysics::moveSigil(idirvec3 dir){
        sigil_->move(dir);

        if(checkCollision() || outOfBounds()){
            //moved but collision occured, moving back.
            sigil_->move(dir.opposite());
            return false; 
        }
                
        return true;// move successful ;)
    }

    bool CSigilPhysics::rotate(){
        sigil_->rotate();
        if(checkCollision() || outOfBounds()){
            fmt::print("collision or outof bounds on rotation, reverting\n");
            sigil_->undoRotate();
            return false;
        }            
        return true;
    }

    bool CSigilPhysics::checkCollision(){
        bool collision = false;
        entity->getManager().forAll<CBlock>([&](unique_ptr<ecs::Entity> &e)  {
            if(e->hasGroup(ecs::Group::BLOCK) && sigil_->collidesWith(&e->getComponent<CBlock>())){
                collision = true;
                return;
            }
        });
        return collision;
    }

    bool CSigilPhysics::outOfBounds(){
        return !frame_.contains(icube(sigil_->bounds().pos, sigil_->bounds().size));
    }
} // chetris
