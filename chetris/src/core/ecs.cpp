#include "core/ecs/ecs.h"

namespace chetris::ecs{
// Here's the definition of `Entity::addToGroup`:
void Entity::addGroup(Group group) noexcept
{
    groupBitset[to_base(group)] = true;
    manager_.addToGroup(this, group);
}
}
