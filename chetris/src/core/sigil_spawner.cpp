#include "core/sigil_spawner.h"

namespace chetris
{
    using namespace ecs;
    
    void SigilSpawner::init(Manager &manager, icube &boardSize, float guiAreaWidth){
        manager_ = &manager;
        frame_ = boardSize;
        guiAreaWidth_ = guiAreaWidth;

        next1_ = &createRandom(frame_.height()-3);
        next2_ = &createRandom(frame_.height()-6);
    }

    Entity* SigilSpawner::spawn(){
        fmt::print("spawning\n");
        
        auto next2Pos = next2_->getComponent<CSigil>().position();
        auto& tail = createRandom(next2Pos.y);
        
        next2_->getComponent<CSigil>().position(vec3(next2Pos.x, next1_->getComponent<CSigil>().position().y, next1_->getComponent<CSigil>().position().z));

        auto &next1Sigil =  next1_->getComponent<CSigil>();       
        next1Sigil.scale(1.f);
	    auto spawnPosition = vec3(frame_.width() / 2.f - next1Sigil.width(), frame_.height() - next1Sigil.height(), (frame_.depth()/2.f) - next1Sigil.depth());
        next1Sigil.position(spawnPosition);
        next1_->delGroup(Group::NEXT_SIGILS);
        next1_->addGroup(Group::SIGIL);
        next1_->addComponent<CSigilPhysics>(frame_);
        next1_->addComponent<CSigilControl>();    
        
        auto spawned = next1_;        
        next1_ = next2_;
        next2_ = &tail;
        
        return spawned;
    }

    Entity &SigilSpawner::createRandom(float y){
        Entity &e = manager_->addEntity();
        e.addGroup(Group::NEXT_SIGILS);

        int numberOfBlockTypes = sizeof(BlockDef::all_types)/sizeof(block_t);
        int r = std::rand() % numberOfBlockTypes;
        BlockDef blockDef = BlockDef::all_types[r];
        fmt::print("blockDef.width()/2.0f = {}\n", blockDef.width()/2.0f);
        float x = (guiAreaWidth_ - blockDef.width()*previewScale)/2.f - guiAreaWidth_;
        
        e.addComponent<CSigil>(blockDef, vec3(x, y, 0.f), 0.f);
        return e;
    }

} // chetris
