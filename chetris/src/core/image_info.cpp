#include "core/util/image_info.h"

#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include "fmt/core.h"

namespace chetris
{

ImageInfo::ImageInfo(const string& filename): fileName_(filename)
{
	stbi_set_flip_vertically_on_load(true);
	data_ = stbi_load(filename.c_str(), &width_, &height_, &numberOfChannels_, 0);
	if (!data_)
	{
		fmt::print("failed to load image: {}\n", filename);
		fmt::print("stb_img error: {}\n", stbi_failure_reason());
	}
}

bool ImageInfo::isOk()
{
	if ((data_ == nullptr) || (width_ == 0) || (height_ == 0))
		return false;
	return true;
}

void ImageInfo::free()
{
	if (data_ != nullptr) {
		stbi_image_free(data_);
		data_ = nullptr;
	}
}
}//namespace
