#include "core/ecs/c_sigil_control.h"
#include <GLFW/glfw3.h>

namespace chetris
{
    void CSigilControl::update(float){}
    
    void CSigilControl::onKeyPress(int key, bool){
        switch (key)
        {
            case GLFW_KEY_LEFT:
                physics_->moveSigil(idirvec3::LEFT);
                break;
            case GLFW_KEY_RIGHT:
                physics_->moveSigil(idirvec3::RIGHT);
                break;
            case GLFW_KEY_UP:
                physics_->moveSigil(idirvec3::BACK);                
                break;
            case GLFW_KEY_DOWN:
                physics_->moveSigil(idirvec3::FRONT);                
                break;
            case GLFW_KEY_ENTER:
                physics_->moveSigil(idirvec3::DOWN);                
                break;
            case GLFW_KEY_SPACE:
                physics_->rotate();
                break;
            default:
                break;
        }
    }
} // chetris
