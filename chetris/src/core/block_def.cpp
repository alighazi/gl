#include "core/block_def.h"

namespace chetris
{

bool BlockDef::at(int y, int x) const{
    assert(x >= 0);
    assert(y >= 0);
    y = SIZE - 1 - y;
    return atIndex(y, x); 
}
void BlockDef::dump(bool raw) const{
    for(int i=0;i<SIZE;i++){
        for(int j=0;j<SIZE;j++){
			if (raw)
				fmt::print("{:d}", atIndex(i, j));
            else
                fmt::print("{:d}", at(i,j));
        }
		fmt::print("\n");
    }
}
void BlockDef::initOnes(){
    int first_nonempty_col = -1;
    for(int j=0;j<BlockDef::SIZE;j++){
        for(int i=0;i<BlockDef::SIZE;i++){            
            if(at(i, j) && first_nonempty_col <0){
                first_nonempty_col = j;
                break;
            }
        }
    }
    int first_nonempty_row = -1;
    ones_.clear();
    for(int i=0;i<BlockDef::SIZE;i++){
        for(int j=0;j<BlockDef::SIZE;j++){
            if(at(i,j)){
                if(first_nonempty_row < 0){
                    first_nonempty_row = i;
                }
                int y =  i - first_nonempty_row;
                int x =  j - first_nonempty_col;
                ones_.insert(ivec2(x, y));
            }
        }
    }
}

BlockDef::BlockDef(block_t blocks):blocks(blocks){
    initOnes();
}
BlockDef::BlockDef(const BlockDef &copy): BlockDef(copy.blocks){}
BlockDef& BlockDef::operator=(const BlockDef that)    {
    blocks = that.blocks;
    initOnes();
    return *this;
}

BlockDef BlockDef::rotate(){
    block_t rotated = 0;
    for(int x = 0; x < SIZE; x++)
    {
        for(int y = 0; y < SIZE; y++)
        {
            int newY = x;
            int newX = SIZE - 1 - y;
            if(atIndex(y, x)){
                int index = SIZE * newY + newX;
                rotated |= (1 << (sizeof(block_t)*CHAR_BIT -1 -index)); 
            }       
        }
        
    }
    return BlockDef(rotated);
}
/*
j = column = x
i = row = y
*/
int BlockDef::width() const{
    int start = SIZE, end = 0;
    for(int j = 0; j < BlockDef::SIZE; j++)
    {
        for(int i = 0; i < BlockDef::SIZE; i++)
        {
            if(atIndex(i, j)){
                if(j<start) start = j;
                else if(j>end) end = j;
                else break;
            }
        }
    }
    return end - start + 1;
}

int BlockDef::height() const {
    int start = SIZE, end = 0;
    for(int i = 0; i < BlockDef::SIZE; i++)
    {
        for(int j = 0; j < BlockDef::SIZE; j++)
        {
            if(atIndex(i,j)){
                if(i<start) start = i;
                else if(i>end) end = i;
            }
        }
    }
    return end - start +1;
}
bool BlockDef::atIndex(int row, int col) const{
    block_t i = row * SIZE + col;    
    return blocks & (1 << (sizeof(block_t)*CHAR_BIT - 1 - i)); 
}

}//namespace
