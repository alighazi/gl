#include "core/camera.h"
#include "core/game.h"

namespace chetris 
{

void Camera::move(float dt, bool left, bool right, bool up, bool down)
{
	float cameraSpeed = speed * dt;
	if (up)
		pos_ += cameraSpeed *  front_;
	if (down)
		pos_ -= cameraSpeed *  front_;
	if (left)
		pos_ -= normalize(cross(front_,  up_)) * cameraSpeed;
	if (right)
		pos_ += normalize(cross(front_,  up_)) * cameraSpeed;
}

void Camera::lookAt(double xpos, double ypos)
{
	if (firstMouse_)
	{
		 lastX_ = xpos;
		 lastY_ = ypos;
		 firstMouse_ = false;
	}

	float xoffset = xpos -  lastX_;
	float yoffset =  lastY_ - ypos; // Reversed since y-coordinates range from bottom to top
	 lastX_ = xpos;
	 lastY_ = ypos;

	xoffset *= sensitivity;
	yoffset *= sensitivity;
	 yaw_ += xoffset;
	 pitch += yoffset;
	if ( pitch > 89.0f)
		 pitch = 89.0f;
	if ( pitch < -89.0f)
		 pitch = -89.0f;

}

void Camera::zoom(double yOffset)
{
	if ( fov_ >= 1.0f &&  fov_ <= 45.0f)
		 fov_ -= yOffset;
	if ( fov_ <= 1.0f)
		 fov_ = 1.0f;
	if ( fov_ >= 45.0f)
		 fov_ = 45.0f;
}

mat4 Camera::getViewProjection()
{
	getView();
	getProjection();
	return  projection_ * view_;
}

const mat4& Camera::getView()
{
	 front_.x = glm::cos(glm::radians( pitch)) * glm::cos(glm::radians( yaw_));
	 front_.y = glm::sin(glm::radians( pitch));
	 front_.z = glm::cos(glm::radians( pitch)) * glm::sin(glm::radians( yaw_));
	 front_ = normalize(front_);
	 view_ = glm::lookAt(pos_, pos_ +  front_,  up_);
	return view_;
}

const mat4& Camera::getProjection()
{
	projection_ = glm::perspective( fov_,  static_cast<float>(windowWidth_) /  windowHeight_, 0.01f, 100.0f);
	return projection_;
}

const vec3& Camera::getPos() const
{
	return pos_;
}

const vec3& Camera::getFront() const
{
	return front_;
}

void Camera::resizeWindow(int windowWidth, int windowHeight) 
{
	windowWidth_ = windowWidth;
	windowHeight_ = windowHeight;
}

void Camera::update(float dt){
	if(Game::instance().isKeyPressed(GLFW_KEY_W))
		move(dt,false, false, true, false);
	else if(Game::instance().isKeyPressed(GLFW_KEY_S))
		move(dt,false, false, false, true);

	if(Game::instance().isKeyPressed(GLFW_KEY_A))
		move(dt, true, false, false, false);
	else if(Game::instance().isKeyPressed(GLFW_KEY_D))
		move(dt, false, true, false, false);
}

void Camera::switchView(){
	
}


}//namespace
