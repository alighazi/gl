#include "core/ecs/c_block.h"
#include "core/util/to_string.h"
#include "core/gl_renderer.h"

namespace chetris{

CBlock::CBlock(const ivec3 &position, const vec3 &color)
:position_(position), color_(vec4(color,1.f)){
    updateTransform();
    model_ = make_shared<Model>(Model{Mesh::cube(), texture, "", color_});
}

CBlock::CBlock(const ivec3 &position, const vec4 &color)
:position_(position), color_(color){
    updateTransform();
    model_ = make_shared<Model>(Model{Mesh::cube(), texture, "", color_});
}

CBlock::~CBlock() {}

void CBlock::position(const ivec3 &position){
    position_ = position;
    updateTransform();
}

void CBlock::updateTransform(){
    transform_ = Transform(position_);

    //positioning should not be dependend of scale
}

void CBlock::highlight(bool val){
    model_->colorDiffuse = val? color_+vec4(0.5f):color_;
}

void CBlock::render(){
    GlRenderer::instance().pushTransform(transform_);    
    GlRenderer::instance().render(model_.get());
    GlRenderer::instance().popTransform();
}

void CBlock::update(float){

}

}//namespace
