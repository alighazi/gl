#include "core/gui.h"
#include "core/game.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_GLFW_GL3_IMPLEMENTATION
#define NK_KEYSTATE_BASED_INPUT
#include "gui/nuklear.h"
#include "gui/nuklear_glfw_gl3.h"
#include "gui/style.h"
//#include "gui/overview.h"

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

namespace chetris 
{

struct nk_context *ctx;
struct nk_colorf bg;
struct nk_image img;
chetris::Gui::Gui(irect bounds) :bounds_(bounds) {
	fmt::print("creating gui with bounds {}\n", bounds_);
	ctx = nk_glfw3_init(Game::instance().window, NK_GLFW3_INSTALL_CALLBACKS);
	/* Load Fonts: if none of these are loaded a default font will be used  */
	/* Load Cursor: if you uncomment cursor loading please hide the cursor */
	{struct nk_font_atlas *atlas;
	nk_glfw3_font_stash_begin(&atlas);
	struct nk_font *droid = nk_font_atlas_add_from_file(atlas, "data/font/Rasa-Regular.ttf", 18, 0);
	/*struct nk_font *roboto = nk_font_atlas_add_from_file(atlas, "../../../extra_font/Roboto-Regular.ttf", 14, 0);*/
	/*struct nk_font *future = nk_font_atlas_add_from_file(atlas, "../../../extra_font/kenvector_future_thin.ttf", 13, 0);*/
	/*struct nk_font *clean = nk_font_atlas_add_from_file(atlas, "../../../extra_font/ProggyClean.ttf", 12, 0);*/
	/*struct nk_font *tiny = nk_font_atlas_add_from_file(atlas, "../../../extra_font/ProggyTiny.ttf", 10, 0);*/
	/*struct nk_font *cousine = nk_font_atlas_add_from_file(atlas, "../../../extra_font/Cousine-Regular.ttf", 13, 0);*/
	nk_glfw3_font_stash_end();
	nk_style_load_all_cursors(ctx, atlas->cursors);
	nk_style_set_font(ctx, &droid->handle); }

	/* set_style(ctx, THEME_WHITE); */
	set_style(ctx, THEME_RED);
	// set_style(ctx, THEME_BLUE);
	// set_style(ctx, THEME_DARK);

	bg.r = 0.10f, bg.g = 0.18f, bg.b = 0.24f, bg.a = 1.0f;
}
chetris::Gui::~Gui()
{
	nk_glfw3_shutdown();
}
void chetris::Gui::update([[maybe_unused]] float dt) {

}
void chetris::Gui::render() {
	nk_glfw3_new_frame();
	/* GUI */
	int rowHeight=30;
	if (nk_begin(ctx, "", nk_rect(bounds_.left(), bounds_.bottom(), bounds_.width(), rowHeight*2.5),
		NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(ctx, rowHeight, 2);
		nk_label(ctx, "Score:", NK_TEXT_LEFT);
		nk_label(ctx, fmt::format("{}",stats_.score()).c_str(), NK_TEXT_RIGHT);
		
		if (stats_.gameEnd()) {
			nk_layout_row_dynamic(ctx, rowHeight, 1);
			nk_label(ctx, "GAME ENDED!", NK_TEXT_CENTERED);
		}
	}	
	nk_end(ctx);

	if (nk_begin(ctx, "2132", nk_rect(bounds_.left(), bounds_.height()-rowHeight*2, bounds_.width(), 
		rowHeight*2), NK_WINDOW_NO_SCROLLBAR))
	{
		nk_layout_row_dynamic(ctx, rowHeight, 2);
		nk_label(ctx, "FPS:", NK_TEXT_LEFT);
		nk_label(ctx, fmt::format("{:.2f}", fps_).c_str(), NK_TEXT_RIGHT);
	}
	nk_end(ctx);

	// overview(ctx);
	/* IMPORTANT: `nk_glfw_render` modifies some global OpenGL state
	* with blending, scissor, face culling, depth test and viewport and
	* defaults everything back into a default state.
	* Make sure to either a.) save and restore or b.) reset your own state after
	* rendering the UI. */
	/* GUI */
	nk_glfw3_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);
}
}
