//#define SANDBOX
#ifndef SANDBOX
#include "core/game.h"

int main()
{
    chetris::Game &game = chetris::Game::instance();
    game.start(800, 600); 
    // game loop
    while (game.isRunning())
    {
        game.update();
        game.render();
    }
    game.dispose();
    return 0;
}

#else

#include "core/common.h"
#include "core/model.h"
using namespace chetris;

int main()
{
    auto m = Model::generate_cylinder(6, 10.f, 4.f);
    return 0;
}

#endif
