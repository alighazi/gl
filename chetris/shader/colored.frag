#version 330 core
out vec4 color;
in vec2 texCoord;
in vec3 normal;
in vec3 fragPos;

uniform vec3 lightPos;  
uniform vec3 camPos;
uniform vec4 uColor;

void main()
{
    color = uColor;       
}