#version 330 core
out vec4 color;
in vec2 texCoord;
in vec3 normal;
in vec3 fragPos;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform vec3 lightPos;  
uniform vec3 camPos;
uniform vec4 uColor;
uniform bool textured;
uniform bool bumped;

void main()
{
    vec4 t;
    if(textured){
        t = texture(diffuseMap, texCoord);
        if(t.a < 0.9) discard;//some transparency hack            
    }else{
        t = vec4(1.);
    }

    vec3 normal2 = normal;
    if(bumped){
        normal2 = texture(normalMap, texCoord).rgb;
        normal2 = normalize(normal2) * 2. - 1.;
    }
    
    vec3 lightDir = normalize(lightPos - fragPos);
    float diffuse = max(dot(lightDir, normalize(normal2)), 0.) * 0.7;

    vec3 viewDir = normalize(camPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, normal2);
    float specular = pow(max(dot(reflectDir, viewDir), 0.), 32.) * 0.7;

	float ambient = 0.2;
	float light = min( ambient + diffuse + specular, 1.);
	float gray = dot(t.rgb, vec3(0.299, 0.587, 0.114));

    color = vec4(t.rgb * light * uColor.rgb, uColor.a);        
}