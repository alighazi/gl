#pragma once
#include "c_sigil_physics.h"

namespace chetris{

    class CSigilControl : public ecs::Component
    {
    private:
        CSigilPhysics* physics_;
    public:
        CSigilControl(){}
        void init() override { physics_ = &entity->getComponent<CSigilPhysics>(); }
        void update(float dt);  
 		void onKeyPress(int key, bool isRepeat);    
		void onKeyRelease([[maybe_unused]] int key) {};
    };
}
