#pragma once
#include "c_sigil.h"
#include "../direction.h"

namespace chetris
{
    class CSigilPhysics : public ecs::Component
    {
    private:
        CSigil* sigil_;
        icube frame_;
    public:
        CSigilPhysics(const icube& frame):frame_(frame){}   
        void init() override { sigil_ = &entity->getComponent<CSigil>(); }
        bool moveSigil(idirvec3 dir);
        bool rotate();
        void update(float dt);
		bool checkCollision();
        bool outOfBounds();
    };

} // chetris
