#pragma once
#include "core/common.h"
#include "core/transform.h"
#include "ecs.h"
#include "core/model.h"

namespace chetris
{
	class CBlock : public ecs::Component
	{
	private:
		Transform transform_;
		ivec3 position_;
		vec4 color_;
		shared_ptr<Model> model_;
		void updateTransform();
		const string texture = "box_border.png";

	public:
		CBlock(const ivec3 &position, const vec3 &color);
		CBlock(const ivec3 &position, const vec4 &color);
		~CBlock();
		void render();
		void update(float dt);
		void position(const ivec3 &position);
		const inline ivec3& position() const { return position_; }
		const inline vec4& color() const { return color_; }
		void highlight(bool val);
	};
}//namespace
