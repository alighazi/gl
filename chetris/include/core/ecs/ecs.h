#pragma once
#include "core/common.h"
#include "group.h"
#include <bitset>

namespace chetris::ecs{

class Component;
class Entity;
class Manager;

using ComponentID = unsigned;

// Let's hide implementation details into an "Internal" namespace:
namespace Internal
{
    inline ComponentID getUniqueComponentID() noexcept
    {
        static ComponentID lastID{0u};
        return lastID++;
    }
}

template <typename T>
inline ComponentID getComponentTypeID() noexcept
{
    // We an use a `static_assert` to make sure this function
    // is only called with types that inherit from `Component`.
    static_assert(std::is_base_of<Component, T>::value,
        "T must inherit from Component");

    static ComponentID typeID{Internal::getUniqueComponentID()};
    return typeID;
}

constexpr std::size_t maxComponents{32};
using ComponentBitset = std::bitset<maxComponents>;
using ComponentArray = std::array<Component*, maxComponents>;
constexpr std::size_t maxGroups{32};
using GroupBitset = std::bitset<maxGroups>;

class Component
{
public:
    Entity* entity;

    // Let's add a virtual `init` method to our Component
    // class that will be called after the component is
    // added to an entity.
    virtual void init() {}

    virtual void update(float) {}
    virtual void render() {}

    virtual ~Component() {}
};

class Entity
{
private:
    bool alive{true};
    std::vector<std::unique_ptr<Component>> components;

    // Let's add an array to quickly get a component with
    // a specific ID, and a bitset to check the existance of
    // a component with a specific ID.
    ComponentArray componentArray;
    ComponentBitset componentBitset;
    GroupBitset groupBitset;
    Manager& manager_;

public:
    Entity(Manager& mManager) : manager_(mManager) {}
    void update(float dt)
    {
        for(auto& c : components) c->update(dt);
    }
    void render()
    {
        for(auto& c : components) c->render();
    }

    bool isAlive() const { return alive; }
    void destroy() { alive = false; }

 
    template <typename T>
    bool hasComponent() const
    {
        return componentBitset[getComponentTypeID<T>()];
    }

    bool hasGroup(Group group) const noexcept
    {
        return groupBitset[to_base(group)];
    }
    bool hasGroup(unsigned group) const noexcept
    {
        return groupBitset[group];
    }

    // To add/remove group we define some methods that alter
    // the bitset and tell the manager_ what we're doing,
    // so that the manager_ can internally store this entity
    // in its grouped containers.
    // We'll need to define this method after the definition
    // of `Manager`, as we're gonna call `Manager::addtoGroup`
    // here.
    void addGroup(Group group) noexcept;
    void delGroup(Group group) noexcept
    {
        groupBitset[to_base(group)] = false;
        // We won't notify the manager_ that a group has been
        // removed here, as it will automatically remove
        // entities from the "wrong" group containers during
        // refresh.
    }


    template <typename T, typename... TArgs>
    T& addComponent(TArgs&&... mArgs)
    {
        // Before adding a component, we make sure it doesn't
        // already exist by using an assertion.
        assert(!hasComponent<T>());

        T* c(new T(std::forward<TArgs>(mArgs)...));
        c->entity = this;
        std::unique_ptr<Component> uPtr{c};
        components.emplace_back(std::move(uPtr));

        // When we add a component of type `T`, we add it to
        // the bitset and to the array.
        componentArray[getComponentTypeID<T>()] = c;
        componentBitset[getComponentTypeID<T>()] = true;

        // We can now call `Component::init()`:
        c->init();

        return *c;
    }

    template <typename T>
    T& getComponent() const
    {
        // To retrieve a specific component, we get it from
        // the array. We'll also assert its existance.

        assert(hasComponent<T>());
        auto ptr(componentArray[getComponentTypeID<T>()]);
        return *static_cast<T*>(ptr);
    }
    Manager& getManager(){return manager_; }
};

class Manager
{
private:
    std::vector<std::unique_ptr<Entity>> entities;
    // We store entities in groups by creating "group buckets" in an
    // array. `std::vector<Entity*>` could be also replaced for
    // `std::set<Entity*>`.
    std::array<std::vector<Entity*>, maxGroups> groupedEntities;
public:
    void update(float dt)
    {
        for(auto& e : entities) e->update(dt);
    }
    void render()
    {
        for(auto& e : entities) e->render();
    }

    // When we add a group to an entity, we just add it to the
    // correct "group bucket".
    void addToGroup(Entity* mEntity, Group group)
    {
        // It would be wise to either assert that the bucket doesn't
        // already contain `mEntity`, or use a set to prevent duplicates
        // in exchange for less efficient insertion/iteration.

        groupedEntities[to_base(group)].emplace_back(mEntity);
    }

    // To get entities that belong to a certain group, we can simply
    // get one of the "buckets" from the array.
    std::vector<Entity*>& getEntitiesByGroup(Group group)
    {
        return groupedEntities[to_base(group)];
    }

    void refresh()
    {
        // During refresh, we need to remove dead entities and entities
        // with incorrect groups from the buckets.
        for(auto i(0u); i < maxGroups; ++i)
        {
            auto& v(groupedEntities[i]);

            v.erase(std::remove_if(std::begin(v), std::end(v),
                        [i](Entity* mEntity)
                        {
                            return !mEntity->isAlive() ||
                                    !mEntity->hasGroup(i);
                        }),
                std::end(v));
        }

        entities.erase(
            std::remove_if(std::begin(entities), std::end(entities),
                [](const std::unique_ptr<Entity>& mEntity)
                {
                    return !mEntity->isAlive();
                }),
            std::end(entities));
    }

    Entity& addEntity()
    {
        Entity* e(new Entity(*this));
        std::unique_ptr<Entity> uPtr{e};
        entities.emplace_back(std::move(uPtr));
        return *e;
    }

    // Call `mFunction` on every object which has component of type T
    template <typename T, typename TFunction>
    void forAll(TFunction&& mFunction)
    {
        for(auto& e : entities)
            if(e->hasComponent<T>())
                mFunction(e);
    }

    // Call `mFunction` on every object which has component of type T1 and T2
    template <typename T1, typename T2, typename TFunction>
    void forAll(TFunction&& mFunction)
    {
        for(auto& e : entities)
            if(e->hasComponent<T1>() && e->hasComponent<T2>())
                mFunction(e);
    }

    // Call `mFunction` on every object which has component of type T1, T2 and T3
    template <typename T1, typename T2, typename T3, typename TFunction>
    void forAll(TFunction&& mFunction)
    {
        for(auto& e : entities)
            if(e->hasComponent<T1>() && e->hasComponent<T2>() && e->hasComponent<T3>())
                mFunction(e);
    }
};

}//namespace
