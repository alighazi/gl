#pragma once
#include "core/common.h"
#include "c_block.h"
#include "core/block_def.h"
#include "core/transform.h"
#include "core/cube.h"

namespace chetris
{
	class CSigil : public ecs::Component
	{
	private:
		BlockDef definition_;
		vec3 position_;
		cube bounds_;
		float scale_;	
		Transform transform_;

		vector<unique_ptr<CBlock>> blocks_;
		void initBlocks();
		void updateTransform();
	public:
		CSigil(const BlockDef definition, const vec3 &pos, float scale = 1.f);
		~CSigil();
		void render();
		void update(float dt);
		const inline vec3 position() const { return position_; }
		void position(const vec3 &position);
		void move(ivec3 dir);
		bool collidesWith(CBlock *b);
		const vector<unique_ptr<CBlock>> &blocks() { return blocks_; }
		void dump();
		void rotate();
		void undoRotate();
		int width();
		int height();
		int depth();
		void scale(float scale);
		const cube& bounds() const {return bounds_;};
	};
}
