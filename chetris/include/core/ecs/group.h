namespace chetris::ecs{
    enum class Group: unsigned {
        BLOCK,
        SIGIL,
        BOARD,
        FRAME,
        NEXT_SIGILS
    };
}
