#pragma once

#include "common.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "ecs/ecs.h"
#include "ecs/c_sigil.h"
#include "gui.h"
#include "stats.h"
#include "sigil_spawner.h"
#include "camera.h"
#include "model.h"

namespace chetris 
{
	class Game
	{
	private:
		//private constructor, we don't want instantiation to be possible
		Game() noexcept;

		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
		static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
		bool keysPressed_[GLFW_KEY_LAST + 1];
		unique_ptr<Gui> gui_;
		unique_ptr<Camera> camera_;
		Stats stats_;
		SigilSpawner sigilSpawner_;
		bool gameEnd_ = false;
		ecs::Manager manager_;		
		// settings
		ivec2 windowSize_;
		icube frame_ = icube(ivec3(0), ivec3(10,20, -10));
		float guiAreaWidth_ = 3.f;
		double deltaTime_ = 0.0f;	// Time between current frame and last frame
    	double lastFrame_ = 0.0f; // Time of last frame
		void checkRowCompletion();
		void mergeSigil(CSigil &sigil);
		void highlightBlocks(CSigil& sigil);

		shared_ptr<Model> ring, box, lamp;

	public:
		GLFWwindow* window;
		void update();
		void render();
		void start(int windowWidth, int windowHeight);
		bool isRunning() noexcept;
		void dispose() noexcept;
		static Game& instance() {
			static Game bp;
			return bp;
		}
		~Game();
		Game(Game const&) = delete;
		void operator=(Game const&) = delete;
		inline bool isKeyPressed(int key) noexcept { return keysPressed_[key]; }
		const inline unique_ptr<Gui>& gui() const noexcept {return gui_;}
		const inline unique_ptr<Camera>& cam() const noexcept {return camera_;}
		const inline Stats& stats() const noexcept {return stats_;}
		inline double time() const noexcept { return lastFrame_; }
	};

}
