#pragma once
#include "common.h"
#include "model.h"
#include "util/shader.h"
#include "core/camera.h"
#include "core/util/image_info.h"
#include "core/transform.h"
#include "core/util/instance_counter.h"

#include <stack>

namespace chetris
{		
	class GlRenderer
	{
	struct Offset{
		int vBO;
		int eBO;
	};
	private:
		GlRenderer(){
			transformStack_.push(Transform());
		}		

		unsigned int vBO_=0, vAO_=0, eBO_=0;
		unordered_map<Mesh*, Offset> offsets;
		unique_ptr<Shader> shader_;
		unordered_map<string, unsigned int> textureNames_;
		std::stack<Transform> transformStack_;
		void loadTexture(const string &fileName);

	public:
		vec4 bgColor = vec4(0.f, 0.f, 0.f, 1.f);
		//todo at some point I want to stop using the singelton pattern here and 
		//just pass an instance of GLRenderer to the CModel (or CMesh)
		static GlRenderer& instance() {
			static GlRenderer bp;
			return bp;
		}
		GlRenderer(GlRenderer const&) = delete;
		void operator=(GlRenderer const&) = delete;

		~GlRenderer();

		void loadMeshs(const vector<Mesh*> &meshs);
		void loadTextures(const vector<string> &textures);
		void setShader(const string &vertexFileName, const string &fragmentFileName);			
		const Shader& shader() const { return *shader_; }
		void begingRender(Camera& cam);

		void dispose() noexcept;
		
		void render(const Model *model);

		void pushTransform(const Transform &transform);
		void popTransform();

	};
}
