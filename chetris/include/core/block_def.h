#pragma once
#include "common.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

namespace chetris {

	typedef unsigned short block_t;
	struct BlockDef
	{
	public:
		static const char SIZE = 4;
		static const block_t TYPE_S = (0b0110 << SIZE * 3)
									+ (0b1100 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_SR = (0b1100 << SIZE * 3)
									+ (0b0110 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_L = (0b0010 << SIZE * 3)
									+ (0b1110 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_LR = (0b1110 << SIZE * 3)
									+ (0b0010 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_Box = (0b1100 << SIZE * 3)
									+ (0b1100 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_Bar = (0b1111 << SIZE * 3)
									+ (0b0000 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_W = (0b1110 << SIZE * 3)
									+ (0b0100 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static const block_t TYPE_M = (0b1110 << SIZE * 3)
									+ (0b1010 << SIZE * 2)
									+ (0b0000 << SIZE * 1)
									+ (0b0000 << SIZE * 0);
		static constexpr block_t all_types[] = { TYPE_S, TYPE_SR, TYPE_L, TYPE_LR, TYPE_Box, TYPE_Bar};

		bool at(int y, int x) const;
		void dump(bool raw = false) const;
		BlockDef(block_t blocks);
		BlockDef(const BlockDef &copy);
		BlockDef& operator=(const BlockDef that);
		BlockDef rotate();
		/*
		j = column = x
		i = row = y
		*/
		int width() const;
		int height() const;
		const inline unordered_set<ivec2>* ones() const { return &ones_; }

	private:
		unordered_set<ivec2> ones_;
		block_t blocks;
		void initOnes();
		bool atIndex(int row, int col) const;
	};
}
