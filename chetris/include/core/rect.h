#pragma once
#include <glm/glm.hpp>
#include "fmt/core.h"

namespace chetris
{
	template <typename T>
	struct rect_t
	{
	private:
		typedef glm::vec<4, T> V4;
		typedef glm::vec<2, T> V2;
		V4 bounds_;
	public:
		rect_t() {}
		rect_t(glm::vec4 bounds) :bounds_(bounds) {}
		rect_t(T top, T right, T bottom, T left) :bounds_(top, right, bottom, left) {}
		rect_t(V2 position, T w, T h) {
			left(position[0]);
			bottom(position[1]);
			this->width(w);
			this->height(h);
		}

		inline const T top() const { return bounds_[0]; }
		inline const T right() const { return bounds_[1]; }
		inline const T bottom() const { return bounds_[2]; }
		inline const T left() const { return bounds_[3]; }

		inline void top(T v) { bounds_[0] = v; }
		inline void right(T v) { bounds_[1] = v; }
		inline void bottom(T v) { bounds_[2] = v; }
		inline void left(T v) { bounds_[3] = v; }

		inline T width() const { return glm::abs(right() - left()); }
		inline T height() const { return glm::abs(top() - bottom()); }

		inline void width(T v) { right(left() + v); }
		inline void height(T v) { top(bottom() + v); }

		inline V2 pos() const { return V2(left(), bottom()); }
		inline void pos(const V2& position) { bounds_ = V4(position.y + height(), position.x + width(), position.y, position.x); }

		inline void move(const V2 &distance) { bounds_ = V4(top() + distance.y, right() + distance.x, bottom() + distance.y, left() + distance.x); }
		inline bool collides(const rect_t<T> &r) const {
			return (r.left() < right() && r.right() > left())
				&& (r.bottom() < top() && r.top() > bottom());
		}
		inline bool collides(const V2 &point) const { return point.x > left() && point.x < right() && point.y > bottom() && point.y < top(); }
	};

	typedef rect_t<float> rect;
	typedef rect_t<int> irect;
}

namespace fmt {
	template <>
	struct formatter<chetris::irect> {
		template <typename ParseContext>
		constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const chetris::irect &r, FormatContext &ctx) {
			return format_to(ctx.out(), "{{top: {}, right: {}, bottom: {}, left: {}}}", r.top(), r.right(), r.bottom(), r.left());
		}
	};
}
