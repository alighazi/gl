#pragma once
#include <glm/glm.hpp>

namespace chetris {
	using glm::vec3;
	using glm::vec2;
	struct Vertex
	{
		vec3 position;
		vec3 normal;
		vec2 tex_coords;
	};
}
