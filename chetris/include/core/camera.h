#pragma once

#include "common.h"
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace chetris
{
	class Camera {

	private:
		mat4 view_, projection_;
		float fov_ = 45.f;
		float yaw_ = -90.f, pitch = -60.f;
		vec3 pos_ = vec3(5.f, 30.f, 15.f);
		vec3 front_ = vec3(0.0f, -0.5f, -1.0f);
		vec3 up_ = vec3(0.0f, 1.0f, 0.0f);
		float lastX_ = 0, lastY_ = 0;
		bool firstMouse_ = true;
		int windowWidth_, windowHeight_;

	public:
		float speed = 10;
		float sensitivity = 0.1f;

		Camera(int windowWidth, int windowHeight)
			: windowWidth_(windowWidth), windowHeight_(windowHeight) {}
		void move(float dt, bool left, bool right, bool up, bool down);
		void lookAt(double xpos, double ypos);
		void zoom(double yOffset);
		mat4 getViewProjection();
		const vec3& getPos() const;
		const vec3& getFront() const;
		const mat4& getView();
		const mat4& getProjection();
		void resizeWindow(int windowWidth, int windowHeight);
		void update (float dt);
		void switchView();
	};
}
