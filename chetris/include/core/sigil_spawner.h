#pragma once
#include "common.h"
#include "ecs/ecs.h"
#include "core/ecs/c_block.h"
#include "core/ecs/c_sigil.h"
#include "core/ecs/c_sigil_physics.h"
#include "core/ecs/c_sigil_control.h"

namespace chetris
{
    class SigilSpawner
    {
    private:
        ecs::Entity* next1_;
        ecs::Entity* next2_;
        float previewScale = 0.5f;
        icube frame_;
        float guiAreaWidth_;
        ecs::Manager *manager_;
        ecs::Entity &createRandom(float y);
    public:
        void init(ecs::Manager &manager, icube &boardSize, float guiAreaWidth);
        ecs::Entity* spawn();
    };    
} // chetris
