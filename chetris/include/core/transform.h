#pragma once
#include "common.h"

#include <glm/gtc/matrix_transform.hpp> 

namespace chetris {

	struct Transform {
	public:
		mat4 transform_;

		inline Transform(const mat4 &mat = mat4(1.f)): transform_(mat) { }
		inline Transform(const vec3 &position) { transform_ = glm::translate(mat4(1.0f), position); }
		inline Transform(float x, float y, float z) { transform_ = glm::translate(mat4(1.0f), vec3(x, y, z)); }

		~Transform() = default;

		inline void transform(const mat4 &transform) { transform_ = transform; }
		inline void rotate(const vec3 &axis, float angle) { transform_ = glm::rotate(transform_, angle, axis); }
		inline void scale(const vec3 &dims) { transform_ = glm::scale(transform_, dims); }
		inline void translate(const vec3 &position) { transform_ = glm::translate(transform_, position); }
		inline void clear() { transform_ = mat4(1.f); }
	};
}
