#pragma once

#include "common.h"

namespace chetris
{
    class Stats
    {
    private:
        int lines_ = 0;
        int doubleLines_ = 0;
        int tripleLines_ = 0;
        int quadrupleLines_ = 0;
		bool gameEnd_ = false;
        float frameTime_ = 0.5f;
    public:
        int score(){return (lines_+doubleLines_*4+tripleLines_*9+quadrupleLines_*16) * 100;}
        void addLines(int count){
            if (count >= 4){
                addLines(count-4);
                quadrupleLines_++;
            }
            else if (count==3)
                tripleLines_++;
            else if (count==2)
                doubleLines_++;
            else if (count==1)
                lines_++;   
            frameTime_ -= count*0.02f;           
        }
		void gameEnd(bool b) { gameEnd_ = b; }
		bool gameEnd() const { return gameEnd_; }
        float frameTime() const {return frameTime_; }
    };  
} // chetris
