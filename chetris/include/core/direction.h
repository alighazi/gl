#pragma once
#include "common.h"

namespace chetris {
	
	template <typename T>
	struct dirvec3t : public glm::vec<3, T>
	{
	private:
		typedef glm::vec<3, T> V3;
	public:
		dirvec3t(const V3& vec) : V3(vec) {}

		static const dirvec3t NONE;
		static const dirvec3t RIGHT;
		static const dirvec3t UP;
		static const dirvec3t DOWN;
		static const dirvec3t LEFT;
		static const dirvec3t FRONT;
		static const dirvec3t BACK;

		inline dirvec3t<T> opposite() const {
			return static_cast<dirvec3t<T>>(-(*this));
		}
	};
	typedef dirvec3t<float> dirvec3;
	typedef dirvec3t<int> idirvec3;

	template <typename T>
	struct dirvec2t : public glm::vec<2, T>
	{
	private:
		typedef glm::vec<2, T> V2;
	public:
		dirvec2t(const V2& vec) : V2(vec) {}

		static const dirvec2t NONE;
		static const dirvec2t RIGHT;
		static const dirvec2t UP;
		static const dirvec2t DOWN;
		static const dirvec2t LEFT;

		inline dirvec2t<T> opposite() const {
			return static_cast<dirvec2t<T>>(-(*this));
		}
	};
	typedef dirvec2t<float> dirvec2;
	typedef dirvec2t<int> idirvec2;

}//namespace chetris

//fmt formatting helpers <3
namespace fmt {

	template <typename T>
	struct formatter<chetris::dirvec2t<T>> {
		template <typename ParseContext>
		constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const chetris::dirvec2t<T>& r, FormatContext& ctx) {
			return format_to(ctx.out(), "{}", static_cast<glm::vec<2, T>>(r));
		}
	};

	template <typename T>
	struct formatter<chetris::dirvec3t<T>> {
		template <typename ParseContext>
		constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const chetris::dirvec3t<T>& r, FormatContext& ctx) {
			return format_to(ctx.out(), "{}", static_cast<glm::vec<3, T>>(r));
		}
	};
}
