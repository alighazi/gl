#pragma once
#include "common.h"
#include "stats.h"

namespace chetris
{
	class Gui
	{
	private:
		irect bounds_;
		float fps_;
		Stats stats_;
	public:
		Gui(irect bounds);
		~Gui();
		void update(float dt);
		void render();
		inline void bounds(irect val) { bounds_ = val; }
		inline irect bounds() { return bounds_; }

		void fps(float f){fps_ = f;}
		void stats(Stats stats){stats_ = stats;}
	};
}
