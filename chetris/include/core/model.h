#pragma once
#include "common.h"
#include "vertex.h"

namespace chetris
{
	struct Mesh
	{
		vector<Vertex> vertices;
		vector<unsigned> indices;

		static constexpr float rectangle_vertices[] = {
			-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
			0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
			0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
			0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
			-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
			-0.5f, -0.5f, -0.5f,  0.0f, 0.0f
		};

		static constexpr float cube_vertices[] = {
		/*TODO this cube doesn't have the right winding order needed for face culling (CCW)
			pos.x   pos.y   pos.z   norm.x  norm.y  norm.z  tex.x   tex.y*/
			-0.5f,	-0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	0.0f,	0.0f,	
			0.5f,	-0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	1.0f,	0.0f,	
			0.5f,	0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	1.0f,	1.0f,	
			0.5f,	0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	1.0f,	1.0f,	
			-0.5f,	0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	0.0f,	1.0f,	
			-0.5f,	-0.5f,	-0.5f,	0.0f,	0.0f,	-1.0f,	0.0f,	0.0f,
				
			-0.5f,	-0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	0.0f,	0.0f,	
			0.5f,	-0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	1.0f,	0.0f,	
			0.5f,	0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	1.0f,	1.0f,	
			0.5f,	0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	1.0f,	1.0f,	
			-0.5f,	0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	0.0f,	1.0f,	
			-0.5f,	-0.5f,	0.5f,	0.0f,	0.0f,	1.0f,	0.0f,	0.0f,	

			-0.5f,	0.5f,	0.5f,	-1.0f,	0.0f,	0.0f,	1.0f,	0.0f,	
			-0.5f,	0.5f,	-0.5f,	-1.0f,	0.0f,	0.0f,	1.0f,	1.0f,	
			-0.5f,	-0.5f,	-0.5f,	-1.0f,	0.0f,	0.0f,	0.0f,	1.0f,	
			-0.5f,	-0.5f,	-0.5f,	-1.0f,	0.0f,	0.0f,	0.0f,	1.0f,	
			-0.5f,	-0.5f,	0.5f,	-1.0f,	0.0f,	0.0f,	0.0f,	0.0f,	
			-0.5f,	0.5f,	0.5f,	-1.0f,	0.0f,	0.0f,	1.0f,	0.0f,	

			0.5f,	0.5f,	0.5f,	1.0f,	0.0f,	0.0f,	1.0f,	0.0f,	
			0.5f,	0.5f,	-0.5f,	1.0f,	0.0f,	0.0f,	1.0f,	1.0f,	
			0.5f,	-0.5f,	-0.5f,	1.0f,	0.0f,	0.0f,	0.0f,	1.0f,	
			0.5f,	-0.5f,	-0.5f,	1.0f,	0.0f,	0.0f,	0.0f,	1.0f,	
			0.5f,	-0.5f,	0.5f,	1.0f,	0.0f,	0.0f,	0.0f,	0.0f,	
			0.5f,	0.5f,	0.5f,	1.0f,	0.0f,	0.0f,	1.0f,	0.0f,	

			-0.5f,	-0.5f,	-0.5f,	0.0f,	-1.0f,	0.0f,	0.0f,	1.0f,	
			0.5f,	-0.5f,	-0.5f,	0.0f,	-1.0f,	0.0f,	1.0f,	1.0f,	
			0.5f,	-0.5f,	0.5f,	0.0f,	-1.0f,	0.0f,	1.0f,	0.0f,	
			0.5f,	-0.5f,	0.5f,	0.0f,	-1.0f,	0.0f,	1.0f,	0.0f,	
			-0.5f,	-0.5f,	0.5f,	0.0f,	-1.0f,	0.0f,	0.0f,	0.0f,	
			-0.5f,	-0.5f,	-0.5f,	0.0f,	-1.0f,	0.0f,	0.0f,	1.0f,	

			-0.5f,	0.5f,	-0.5f,	0.0f,	1.0f,	0.0f,	0.0f,	1.0f,	
			0.5f,	0.5f,	-0.5f,	0.0f,	1.0f,	0.0f,	1.0f,	1.0f,	
			0.5f,	0.5f,	0.5f,	0.0f,	1.0f,	0.0f,	1.0f,	0.0f,	
			0.5f,	0.5f,	0.5f,	0.0f,	1.0f,	0.0f,	1.0f,	0.0f,	
			-0.5f,	0.5f,	0.5f,	0.0f,	1.0f,	0.0f,	0.0f,	0.0f,	
			-0.5f,	0.5f,	-0.5f,	0.0f,	1.0f,	0.0f,	0.0f,	1.0f
		};

		static shared_ptr<Mesh> cube();
		static shared_ptr<Mesh> generate_torus(unsigned sectionsBig=18, unsigned sectionsSmall =18, float radiusBig = 10.f, float radiusSmall = 3.f);
		static shared_ptr<Mesh> generate_ring(unsigned sections, float radius, float height);
		static shared_ptr<Mesh> generate_cylinder(unsigned sections, float radius, float height);
		static void generate_sphere();

	};

	struct Model
	{
		shared_ptr<Mesh> mesh;
		string diffuseMapFn;
		string normalMapFn;
		vec4 colorDiffuse;	
		bool isWireFrame = false;
	};
}
