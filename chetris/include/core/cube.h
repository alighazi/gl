#pragma once
#include <glm/glm.hpp>
#include "fmt/core.h"
#include "core/util/to_string.h"

#ifdef _WIN32 //these will cause name clash #FUCK
#undef near
#undef far
#endif

namespace chetris
{
	template <typename T>
	struct cubet
	{
	private:
		typedef glm::vec<3, T> V3;
	public:
		V3 pos;
		V3 size;

		cubet() {}
		cubet(const V3& position, const V3& size) : pos(position), size(size) {}

		inline const T top() const { return this->max().y; }
		inline const T right() const { return this->max().x; }
		inline const T bottom() const { return this->min().y; }
		inline const T left() const { return this->min().x; }
		inline const T near() const {return this->max().z;}
		inline const T far() const { return this->min().z; }

		inline V3 min() const {
			V3 end = pos + size;
			return V3(glm::min(pos.x, end.x), glm::min(pos.y, end.y), glm::min(pos.z, end.z));
		}

		inline V3 max() const {
			V3 end = pos + size;
			return V3(glm::max(pos.x, end.x), glm::max(pos.y, end.y), glm::max(pos.z, end.z));
		}

		inline T width() const { return size.x; }
		inline T height() const { return size.y; }
		inline T depth() const { return size.z; }

		inline void width(T v) { size.x = v; }
		inline void height(T v) { size.y = v; }
		inline void depth(T v) { size.z = v; }

		inline void move(const V3& distance) { pos += distance; }

		inline bool collides(const cubet<T>& c) const {
			V3 end1 = pos + size;
			V3 min1 = V3(glm::min(pos.x, end1.x), glm::min(pos.y, end1.y), glm::min(pos.z, end1.z));
			V3 max1 = V3(glm::max(pos.x, end1.x), glm::max(pos.y, end1.y), glm::max(pos.z, end1.z));

			V3 end2 = c.pos + c.size;
			V3 min2 = V3(glm::min(c.pos.x, end2.x), glm::min(c.pos.y, end2.y), glm::min(c.pos.z, end2.z));
			V3 max2 = V3(glm::max(c.pos.x, end2.x), glm::max(c.pos.y, end2.y), glm::max(c.pos.z, end2.z));

			return (min2.x <= max1.x && max2.x >= min1.x)
				&& (min2.y <= max1.y && max2.y >= min1.y)
				&& (min2.z <= max1.z && max2.z >= min1.z);
		}
		
		inline bool contains(const cubet<T>& c) const {
			V3 end1 = pos + size;
			V3 min1 = V3(glm::min(pos.x, end1.x), glm::min(pos.y, end1.y), glm::min(pos.z, end1.z));
			V3 max1 = V3(glm::max(pos.x, end1.x), glm::max(pos.y, end1.y), glm::max(pos.z, end1.z));

			V3 end2 = c.pos + c.size;
			V3 min2 = V3(glm::min(c.pos.x, end2.x), glm::min(c.pos.y, end2.y), glm::min(c.pos.z, end2.z));
			V3 max2 = V3(glm::max(c.pos.x, end2.x), glm::max(c.pos.y, end2.y), glm::max(c.pos.z, end2.z));

			auto result = (min2.x <= max1.x	&& max2.x >= min1.x && min2.x >= min1.x && max2.x <= max1.x
				&& 	min2.y <= max1.y && max2.y >= min1.y && min2.y >= min1.y && max2.y <= max1.y
				&& 	min2.z <= max1.z && max2.z >= min1.z && min2.z >= min1.z && max2.z <= max1.z);
			return result;
		}

		inline bool collides(const V3& point) const {
			V3 end = pos + size;
			V3 min = V3(glm::min(pos.x, end.x), glm::min(pos.y, end.y), glm::min(pos.z, end.z));
			V3 max = V3(glm::max(pos.x, end.x), glm::max(pos.y, end.y), glm::max(pos.z, end.z));

			return point.x >= min.x	&& point.x <= max.x
				&& point.y >= min.y	&& point.y <= max.y
				&& point.z >= min.z	&& point.z <= max.z;
		}
	};

	typedef cubet<float> cube;
	typedef cubet<int> icube;
}

namespace fmt {
	template <typename T>
	struct formatter<chetris::cubet<T>> {
		template <typename ParseContext>
		constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

		template <typename FormatContext>
		auto format(const chetris::cubet<T>& c, FormatContext& ctx) {
			return format_to(ctx.out(), "pos: {} size: {}", c.pos, c.size);
		}
	};
}
