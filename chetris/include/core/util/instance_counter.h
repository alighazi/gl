#pragma once
#include "fmt/core.h"

/* this class can be used to wrap around an object and keep track of its instance initializations
helpful for finding leaks and excessive copies/moves etc. */

template <typename T>
struct instance_counter {

    instance_counter() noexcept { ++icounter.num_construct; }
    instance_counter(const instance_counter&) noexcept { ++icounter.num_copy; }
    instance_counter(instance_counter&&) noexcept { ++icounter.num_move; }
    // Simulate both copy-assign and move-assign
    instance_counter& operator=(instance_counter) noexcept
    {
        return *this;
    }
    ~instance_counter() { ++icounter.num_destruct; }

private:
    static struct counter {
        int num_construct = 0;
        int num_copy = 0;
        int num_move = 0;
        int num_destruct = 0;

        ~counter()
        {
            fmt::print("{} direct constructions\n", num_construct);
            fmt::print("{} copies\n", num_copy);
            fmt::print("{} moves\n", num_move);
            const int total_construct = num_construct + num_copy + num_move;
            fmt::print("{} total constructions\n", total_construct);
            fmt::print("{} destructions ", num_destruct);
            if (total_construct == num_destruct) {
                fmt::print("(no leaks)\n");
            } else {
                fmt::print("WARNING: potential leak");
            }
        }
    } icounter;
};

template <typename T>
typename instance_counter<T>::counter instance_counter<T>::icounter{};

template <typename T>
struct counted : T, private instance_counter<T>
{
    using T::T;
};
