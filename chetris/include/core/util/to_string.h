#pragma once
#include <vector>
#include <glm/glm.hpp>
#include "fmt/core.h"

#include "core/vertex.h"

namespace std{

    // string to_string(const glm::vec3 &v){
    //     return fmt::format("[{:.4}, {:.4}, {:.4}]", v.x,v.y,v.z);
    // }
    // string to_string(const glm::vec2 &v){
    //     return fmt::format("[{:.4}, {:.4}]", v.x,v.y);
    // }

    // string to_string(std::vector<glm::vec3> &v){
    //     string s="";
    //     for(int i=0;i<v.size();i++){
    //         if(i>0) s+="\n";
    //         s+= fmt::format("{}/{}=> [{:.4}, {:.4}, {:.4}]", i+1,v.size(), v[i].x,v[i].y,v[i].z);
    //     }
    //     return s;
    // }

    // template<class T>
    // string to_string_vscalar(std::vector<T> &v){
    //     string s=fmt::format("{}:[",v.size());
    //     for(int i=0;i<v.size();i++){
    //         if(i>0) s+=", ";
    //         s+=fmt::format("{}",v[i]);
    //     }
    //     return s+"]";
    // }

    // template<class T>
    // string to_string_vector_float(std::vector<T> &v, int precision){
    //     string s=fmt::format("{}:[",v.size());
    //     for(int i=0;i<v.size();i++){
    //         if(i>0) s+=", ";
    //         s+=fmt::format("{:.{}f}",v[i], precision);
    //     }
    //     return s+"]";
    // }
    
    // string to_string(std::vector<unsigned int> &v){
    //     return to_string_vscalar(v);
    // }
    // string to_string(std::vector<int> &v){
    //     return to_string_vscalar(v);
    // }
    // string to_string(std::vector<char> &v){
    //     return to_string_vscalar(v);
    // }
    // string to_string(std::vector<long> &v){
    //     return to_string_vscalar(v);
    // }
    // string to_string(std::vector<float> &v, int precision = 4){
    //     return to_string_vector_float(v, precision);
    // }
    // string to_string(std::vector<double> &v, int precision = 8){
    //     return to_string_vector_float(v, precision);
    // }

}

namespace fmt {

using chetris::Vertex;

template <>
struct formatter<glm::ivec2> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::ivec2 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{}, {}", r.x, r.y);
  }
};

template <>
struct formatter<glm::ivec3> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::ivec3 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{}, {}, {}", r.x, r.y, r.z);
  }
};


template <>
struct formatter<glm::vec2> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::vec2 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{:.3f}, {:.3f}", r.x, r.y);
  }
};

template <>
struct formatter<glm::vec3> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::vec3 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{:.3f}, {:.3f}, {:.3f}", r.x, r.y, r.z);
  }
};

template <>
struct formatter<glm::vec4> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::vec4 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{:.3f}, {:.3f}, {:.3f}, {:.3f}", r.x, r.y, r.z, r.w);
  }
};

template <>
struct formatter<glm::mat4> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const glm::mat4 &r, FormatContext &ctx) {
    return format_to(ctx.out(), "{:.3f}, {:.3f}, {:.3f}, {:.3f}\n{:.3f}, {:.3f}, {:.3f}, {:.3f}\n{:.3f}, {:.3f}, {:.3f}, {:.3f}\n{:.3f}, {:.3f}, {:.3f}, {:.3f}\n",
    r[0][0], r[1][0], r[2][0], r[3][0],
    r[0][1], r[1][1], r[2][1], r[3][1],
    r[0][2], r[1][2], r[2][2], r[3][2],
    r[0][3], r[1][3], r[2][3], r[3][3]);
  }
};

template <>
struct formatter<Vertex> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const Vertex &v, FormatContext &ctx) {
    return format_to(ctx.out(), "position: {}, tex_coords: {}, color: {}", v.position, v.tex_coords, v.normal);
  }
};

}
