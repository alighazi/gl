#pragma once
#include "core/common.h"
namespace chetris
{

class ImageInfo {

public:
	ImageInfo(const string& fileName);
	const string& getFileName() const {return fileName_;}
	bool isOk();
	void free();
	inline int width() const {return width_;}
	inline int height() const {return height_;}
	inline int numberOfChannels() const {return numberOfChannels_;}
	inline unsigned char* pixels()const {return data_;}
	inline ~ImageInfo() {free();}

private:
	int width_ = 0;
	int height_ = 0;
	unsigned char *data_ = nullptr;
	int numberOfChannels_ = 0;
	const string fileName_;
};

}//namespace
