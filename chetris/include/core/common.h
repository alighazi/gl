#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <memory>
#include <algorithm>
#include <cassert>
//we use fmt everywhere instead of iostream for its performance and flexiblity
#include "fmt/core.h"
#include "util/to_string.h"
#include "util/make_vector.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "rect.h"
#include "cube.h"
#include "direction.h"

namespace chetris
{
    using std::string;  
    using std::vector;
    using std::array;
    using std::unordered_set;
    using std::unordered_map;
    using std::unique_ptr;
    using std::make_unique;

    using std::shared_ptr;
    using std::make_shared;
    using chetris::util::make_vector;
    
    using glm::ivec2;
    using glm::ivec3;
    using glm::vec2;
    using glm::vec3;
    using glm::vec4;
    using glm::mat4;

    template <typename E>
    constexpr typename std::underlying_type<E>::type to_base(E e) noexcept {
        return static_cast<typename std::underlying_type<E>::type>(e);
    }
}
